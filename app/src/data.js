"use strict";

function applyEffect(id, player) {
    effect[id].apply(player);
    player.effects.push(id);
}

var generic_item = {
    'hand-sanitizer': {
        tooltip: 'Raises hygiene by 1.',
        duration: 2000,
        use: function (player) {
            player.needs.HYG += 1;
        }
    },
    'energy-drink': {
        tooltip: 'Raises stamina by 2, but reduces INT by 2 until you sleep.',
        duration: 2000,
        use: function (player) {
            player.needs.STA += 2;
            applyEffect('hyper', player);
        }
    }
}

var item = {
    'Original Hand Sanitizer': {
        info: generic_item['hand-sanitizer'],
        icon: 'assets/icons/hand_sanitizer_original.png'
    },
    'Red Heifer': {
        info: generic_item['energy-drink'],
        icon: 'assets/icons/energy_drink_red_heifer.png'
    }
}

var action = {
    'sleep': function (player, quality) {
        player.effects = player.effects.filter(id => {
            var sleep = effect[id].remove.sleep;
            if (sleep) return sleep(player);
            return true;
        });
        quality = quality || 10;
        player.needs.STA += 10;
    },
    'eat': function (player, quality) {
        quality = quality || 2;
        player.needs.HUN += quality;
    },
    'clean': function (player, quality) {
        quality = quality || 1;
        player.needs.HYG += quality;
    }
}

var effect = {
    'hyper': {
        tooltip: '-2 INT (recover by sleeping)',
        apply: function (player) {
            player.traits.INT -= 2;
        },
        remove: {
            sleep: function (player) {
                player.traits.INT += 2;
            }
        }

    }
}

module.exports = {item, action, effect};

"use strict";

var seedrandom = require('seedrandom');

var rng = null;
function generate(min, max) {
    if ( ! rng ) throw new Error('RNG not seeded.');
    if ( !min && !max ) return rng();
    var rand = min + rng()*(max+1-min)
    return Math.floor(rand);
}

function seed(seed) {
    rng = seedrandom(seed);
}

module.exports = {random: {generate, seed}};

"use strict";

var netsync = require('netsync');
var render = require('../render/render.js');
var constants = require('../const.js');
var Sprite = require('../render/obj.js');
var Matrix4 = require('../render/cuon-matrix.js');

var data = require('../data.js');

var icons = {
    'eat': 'assets/icons/eat_icon.png',
    'soap': 'assets/icons/soap_icon.png'
}

var configs = {
    'home': [
        {image: icons['eat'], option: 'eat'},
        {image: icons['soap'], option: 'clean'},
        {image: icons['soap'], option: 'sleep'}
    ]
}

module.exports = class PlayerCharacter extends netsync.BaseEntity {
    init(p) {
        this.map(['x', 'y', 'direction', 'needs', 'traits', 'ui', 'inventory', 'action', 'effects']);

        this.x = p.x || 2;
        this.y = p.y || 2;
        this.direction = p.direction || [1,0];
        this.needs = p.needs || {HUN: 7, STA: 7, HYG: 7};
        this.traits = p.traits || {STR: 2, INT: 2};
        this.ui = p.ui || {};
        this.inventory = p.inventory || {items: ['Original Hand Sanitizer', 'Original Hand Sanitizer', 'Original Hand Sanitizer', 'Original Hand Sanitizer', 'Red Heifer', 'Red Heifer'], coins: 10};
        this.action = p.action || {};
        this.effects = p.effects || [];

        this.setOwner(p.owner);
        if (this.notRemote()) UI.setState(this.getState());
        this.speed = 2;
        this.sprite = new Sprite(1, 1, 1, 1, 'pixx', -3);
    }

    getState() {
        return {needs: this.needs, traits: this.traits, ui: this.ui, inventory: this.inventory, action: this.action, effects: this.effects};
    }

    handleEvent(event) {
        switch (event.name) {
            case 'tick':
                var ms = event.ms;
                if (this.action.name) {
                    this.action.progress += (ms/this.action.duration)*100;
                    if (this.action.progress > 100) this.completeAction();
                }
                break;
        }
    }

    completeAction() {
        var name = this.action.name;
        var info = this.action.info;
        switch (name) {
            case 'eat':
                data.action.eat(this);
                break;
            case 'clean':
                data.action.clean(this);
                break;
            case 'sleep':
                data.action.sleep(this);
                break;
            case 'use':
                var selected = info;
                var name = this.inventory.items[selected];
                var item = data.item[name].info;
                item.use(this);
                this.inventory.items.splice(selected, 1);
                break;
        }
        delete this.action.name;
        delete this.action.info;
        delete this.action.duration;
        delete this.action.progress;
        this.normalize();
        console.log('Action completed...');
    }

    startAction(name, info, duration) {
        this.action.name = name;
        this.action.info = info;
        this.action.duration = duration;
        this.action.progress = 0;
    }

    normalize() {
        for (var need in this.needs) {
            this.needs[need] = Math.min(10, this.needs[need]);
        }
    }

    processInput(playerId, input) {
        if (this.owner != playerId) return;
        if (this.action.name) return;

        if (this.ui.options || this.ui.inventory) this.handleUI(input);
        else                                      this.handleCharacter(input);

        this.normalize();
    }

    handleUI(input) {
        if (input.down && input.key == 'enter') {
            if (this.ui.options) {
                var option = this.ui.options[this.ui.selected].option;
                console.log('SELECTED OPTION:', option);
                this.startAction(option, null, 2000);
                this.resetUI();
                return;
            }
            if (this.ui.inventory) {
                var item = this.inventory.items[this.ui.selected];
                console.log('SELECTED ITEM:', item);
                // Watch out for potential race conditions, here
                // (item might not be there when the action is
                // complete, for whatever reason)
                this.startAction('use', this.ui.selected, data.item[item].info.duration);
                this.resetUI();
                return;
            }
        }

        if (this.ui.options || this.ui.inventory) {
            if (input.down) {
                var n;
                if (this.ui.options) n = this.ui.options.length;
                else n = this.inventory.items.length;
                if ( !n ) return;
                switch (input.key) {
                    case 'a':
                        this.ui.selected = Math.max(0, this.ui.selected-1);
                        break;
                    case 'd':
                        this.ui.selected = Math.min(n-1, this.ui.selected+1);
                        break;
                    case 'e':
                        if (this.ui.options) this.resetUI();
                        break;
                    case 'i':
                        if (this.ui.inventory) this.resetUI();
                        break;
                    case 'esc':
                        this.resetUI();
                        break;
                }
            }
        }
    }

    handleCharacter(input) {
        var velocity = [0, 0];
        switch (input.key) {
            case 'w':
                velocity[1] -= (input.ms/1000)*this.speed;
                this.direction = [0,-1];
                break;
            case 'a':
                velocity[0] -= (input.ms/1000)*this.speed;
                this.direction = [-1,0];
                break;
            case 's':
                velocity[1] += (input.ms/1000)*this.speed;
                this.direction = [0,1];
                break;
            case 'd':
                velocity[0] += (input.ms/1000)*this.speed;
                this.direction = [1,0];
                break;
            case 'i':
                if (input.down) this.showUIInventory();
                break;
            case 'e':
                if (input.down) {
                    var buildings = this.world.village.village.getOverlaps(this.x, this.y, 1, 1);
                    if (buildings.length) this.showUIOptions('home');
                }
                break;
        }

        this.x += velocity[0];
        this.y += velocity[1];

        if (this.notRemote()) render.setView(this.x-5, this.y-5);
    }

    resetUI() {
        delete this.ui.inventory;
        delete this.ui.options;
        this.ui.selected = 0;
    }

    showUIOptions(config) {
        this.resetUI();
        this.ui.options = configs[config];
    }

    showUIInventory() {
        this.resetUI();
        this.ui.inventory = true;
    }

    notRemote() { 
        var input = this.world.inputs.get(this.owner);
        return !!input;
    }

    render() {
        var modelMatrix = new Matrix4();
        modelMatrix.setTranslate(this.x, this.y, 0);
        this.sprite.draw(modelMatrix);
    }
}

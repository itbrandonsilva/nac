"use strict";

var netsync = require('netsync');
var render = require('../render/render.js');
var constants = require('../const.js');
var Sprite = require('../render/obj.js');
var Matrix4 = require('../render/cuon-matrix.js');
var Village = require('../village.js');
var utils = require('../utils.js');

module.exports = class World extends netsync.BaseEntity {
    init(params) {
        this.map(['seed']);

        if ( ! this.seed ) {
            this.seed = params.seed || Math.random().toString();
            utils.random.seed(this.seed);
            this.village = new Village();
        }

        this.sprite = new Sprite(1, 1, 40, 40, 'grass');
        this.world.village = this;
    }

    render() {
        var modelMatrix = new Matrix4();
        modelMatrix.setTranslate(0, 0, 0);
        this.sprite.draw(modelMatrix);
        this.village.draw();
    }
}

"use strict";

var utils = require('./utils.js');
var constants = require('./const.js');
var TILE_SIZE = constants.TILE_SIZE;
var Sprite = require('./render/obj.js');
var Matrix4 = require('./render/cuon-matrix.js');

class BSP {
    constructor(width, height, minSize, x, y, parent) {
        this.left = null;
        this.right = null;

        this.minSize = minSize || 14;

        this.width = width;
        this.height = height;

        this.x = x || 0;
        this.y = y || 0;

        this.traverse(this);
        this.parent = parent;
    }

    traverse() {
        var result = this.split();
        if (!result) return;
        this.left.traverse();
        this.right.traverse();
    }

    split() {
        if (this.left || this.right) return false;

        var rand = utils.random.generate();
        var splitH = rand > 0.5;

        // Split where we are widest
        if (this.width / this.height > 1.25) splitH = false;
        if (this.height / this.width > 1.25) splitH = true;

        var max = (splitH ? this.height : this.width) - this.minSize;
        if (max <= this.minSize) return false; // We can't split this leaf any further

        var split = utils.random.generate(this.minSize, max);

        if (splitH) {
            this.left = new BSP(this.width, split, this.minSize, this.x, this.y, this);
            this.right = new BSP(this.width, this.height - split, this.minSize, this.x, this.y + split, this);
        } else {
            this.left = new BSP(split, this.height, this.minSize, this.x, this.y, this);
            this.right = new BSP(this.width - split, this.height, this.minSize, this.x + split, this.y, this);
        }
        return true;
    }

    isLeaf() {
        return (!this.left);
    }

    forEachLeaf(cb) {
        if ( ! this.left ) return cb(this);
        this.left.forEachLeaf(cb);
        this.right.forEachLeaf(cb);
    }

    forEach(cb) {
        cb(this);
        if (this.left) {
            this.left.forEach(cb);
            this.right.forEach(cb);
        }
    }
}

function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

module.exports = class Village {
    constructor(width, height) {
        var bsp = new BSP(width || 40, height || 40, 4);

        var zones = [];
        bsp.forEachLeaf(leaf => {
            zones.push({w: leaf.width, h: leaf.height, x: leaf.x, y: leaf.y});
        });

        //shuffleArray(zones);

        this.buildings = [];
        zones.forEach(zone => {
            var w = 2;
            var h = 2;
            var sprite = new Sprite(w, h, 1, 1, 'red', -1);
            var matrix = new Matrix4();
            matrix.setTranslate(zone.x, zone.y, 0);
            this.buildings.push({sprite, matrix, x: zone.x, y: zone.y, w, h, type: 'vacant'});
        });
    }

    getOverlaps(x, y, w, h) {
        var overlaps = [];
        this.buildings.forEach(b => {
            if (x < b.x+b.w && x+w > b.x && y < b.y+b.h && y+h > b.y) overlaps.push(b);
        });
        return overlaps;
    }

    draw() {
        this.buildings.forEach(building => {
            //console.log(building.
            building.sprite.draw(building.matrix);
        });
    }
}

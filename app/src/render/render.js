"use strict";

var Matrix4 = require('./cuon-matrix.js');
var utils = require('./cuon-utils.js');

var shaders = require('./shaders.js');

var canvas = document.getElementById('game');
var gl = canvas.getContext('webgl');

var w = canvas.width;
var h = canvas.height;

utils.initShaders(gl, shaders.vertex, shaders.fragment);

gl.clearColor(0.1, 0.1, 0.1, 1.0);
gl.enable(gl.DEPTH_TEST);

var projMatrix = new Matrix4();
var W = 10;
var TILE_SIZE = 1;
// left, right, bottom, top, near, far
projMatrix.setOrtho(0, W * TILE_SIZE, -W * TILE_SIZE, 0, -100, 100);

var eyex = 0;
var eyey = 0;
var viewMatrix = new Matrix4();
// eyeX, eyeY, eyeZ, atX, atY, atZ, upX, upY, upZ
viewMatrix.setLookAt(eyex, eyey, -10,  eyex, eyey, 0,  0, -1, 0); 

function updateView() {
    viewMatrix.setLookAt(eyex, eyey, -10,  eyex, eyey, 0,  0, -1, 0); 
}

function translateView(x, y) {
    eyex += x;
    eyey += y;
    updateView();
}

function setView(x, y) {
    eyex = x;
    eyey = y;
    updateView();
}

module.exports = {
    gl, translateView, setView, viewMatrix, projMatrix,
    reset: () => {
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        readInputs();
    }
}

var basex = 150;
var basey = 150;
var eyeheight = 300;
// up, down, left, right
var codes = [38, 40, 37, 39];
var inputs = {};
window.onkeyup = function (e) {
    if (codes.indexOf(e.keyCode) > -1) delete inputs[e.keyCode];
};
window.onkeydown = function (e) {
    //console.log(e.keyCode);
    if (codes.indexOf(e.keyCode) > -1) inputs[e.keyCode] = true;
};
window.onwheel = function (e) {
    e.preventDefault();
    var y = e.wheelDeltaY;
    if (y > 0) eyeheight -= 17;
    else if (y < 0) eyeheight += 17;
};
var speed = 0.1;
function readInputs() {
    if (inputs[37]) eyex -= speed;
    if (inputs[39]) eyex += speed;
    if (inputs[38]) eyey -= speed;
    if (inputs[40]) eyey += speed;
}
readInputs();

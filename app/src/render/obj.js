"use strict";

var OBJ = require('webgl-obj-loader');
var Matrix4 = require('./cuon-matrix.js');
var render = require('./render.js');
var viewMatrix = render.viewMatrix;
var projMatrix = render.projMatrix;

var gl = render.gl;

var images = {};
function loadImage(path) {
    var image = new Image();
    images[path.split('/').pop().split('.').shift()] = image;
    image.onload = () => {
        var texture = gl.createTexture();
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, gl.RGB, gl.UNSIGNED_BYTE, image);
        image.texture = texture;
    };
    image.src = path;
}

loadImage("assets/grass/grass.png");
loadImage("assets/pixx/pixx.png");
loadImage("assets/red/red.png");

module.exports = class Sprite {
    constructor(tw, th, sx, sy, texture, layer) {
        this.gl = render.gl;

        var z = layer || 0;
        this.image = images[texture];

        sx = sx || 10;
        sy = sy || 10;

        var vertices = [];
        var texture = [];

        for (var x = 0; x < sx; ++x) {
            for (var y = 0; y < sy; ++y) {
                vertices = vertices.concat([
                    0.0+x,  th+y, z,
                     tw+x, 0.0+y, z,
                    0.0+x, 0.0+y, z,

                    0.0+x,  th+y, z,
                     tw+x, 0.0+y, z,
                     tw+x,  th+y, z,
                ]);

                texture = texture.concat([
                    0.0, 0.0,
                    1.0, 1.0,
                    0.0, 1.0,

                    0.0, 0.0,
                    1.0, 1.0,
                    1.0, 0.0,
                ]);
            }
        }

        this.vertices = new Float32Array(vertices); 
        this.vertexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, this.vertices, gl.STATIC_DRAW);

        this.texture = new Float32Array(texture);
        this.textureBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.textureBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, this.texture, gl.STATIC_DRAW);
    }

    draw(modelMatrix) {
        var gl = this.gl;
        var texture = this.image.texture;
        if ( ! texture ) return;

        var FSIZE = new Float32Array([]).BYTES_PER_ELEMENT;

        gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBuffer);
        var a_Position = gl.getAttribLocation(gl.program, 'a_Position');
        gl.vertexAttribPointer(a_Position, 3, gl.FLOAT, false, 0, 0); 
        gl.enableVertexAttribArray(a_Position);

        gl.bindBuffer(gl.ARRAY_BUFFER, this.textureBuffer);
        var a_TexCoord = gl.getAttribLocation(gl.program, 'a_TexCoord');
        gl.vertexAttribPointer(a_TexCoord, 2, gl.FLOAT, false, 0, 0); 
        gl.enableVertexAttribArray(a_TexCoord);

        gl.bindTexture(gl.TEXTURE_2D, texture);
        var u_Sampler = gl.getUniformLocation(gl.program, 'u_Sampler');
        gl.uniform1i(u_Sampler, 0);

        var mvpMatrix = new Matrix4();
        mvpMatrix.set(projMatrix).multiply(viewMatrix).multiply(modelMatrix);

        var u_MvpMatrix = gl.getUniformLocation(gl.program, 'u_MvpMatrix');
        gl.uniformMatrix4fv(u_MvpMatrix, false, mvpMatrix.elements);

        gl.drawArrays(gl.TRIANGLES, 0, this.vertices.length/3);
    }
}

"use strict";

var prefix = './entities/';
var entities = [
    'PlayerCharacter.js', 'World.js'
];

module.exports = function (world) {
    entities.forEach(entity => {
        world.registerEntityType(require(prefix + entity));
    });
};

"use strict";

module.exports = {
    PORT: 8085,
    TILE_SIZE: 20,
    CHARACTER_SIZE: 10,
}

"use strict";

import {bootstrap} from 'angular2/platform/browser'
import {Component, Input, NgZone, OnChanges} from 'angular2/core';
import {CORE_DIRECTIVES} from 'angular2/common';

declare function require(name:string);
var netsync = require('netsync');

class GameInput extends netsync.SampleInput {
    constructor() {
        super(['w', 'a', 's', 'd', 'enter', 'i', 'e', 'esc']);
    }
}

@Component({
    selector: 'app',
    templateUrl: 'src/ng/template/app.html'
})
class App implements OnChanges {
    constructor(private _zone: NgZone) {
        this.world = true;
        setTimeout(() => this.world = false, 50);
        window.UI = this;
        this.data = require('./src/data.js');
    }

    // http://stackoverflow.com/questions/34434057/angular-2-x-watching-for-variable-change
    ngAfterContentChecked() {
        this.state = this.state;
    }

    onGo(form) {
        var ip = form.value.ip;
        var isServer = form.value.server;
        if ( !ip || isServer == null ) return;
        isServer = !!parseInt(isServer);
        this.HOST = ip;
        this.start(isServer);
    }

    start(isServer, ip) {
        var world = new (require('./src/world.js'))(isServer, false);
        var entities = require('./src/entities.js')(world);

        var PORT = 8085;
        var gateway = new netsync.TCPGateway(PORT, null, this.HOST);
        world.setGateway(gateway);
        gateway.enable();

        world.start(1000/30);
        this.world = world;
    }

    setState(state) {
        this.state = state;
    }
}

bootstrap(App, [CORE_DIRECTIVES]);

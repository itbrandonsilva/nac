"use strict";

var netsync = require('netsync');
var render = require('./render/render.js');
var utils = require('./utils.js');

class GameInput extends netsync.SampleInput {
    constructor() {
        super(['w', 'a', 's', 'd', 'enter', 'i', 'e', 'esc']);
    }
}

module.exports = class World extends netsync.BaseWorld {
    onMessage(msg, conn) {
        switch (msg.type) {
            case 'join':
                this.addPlayer(null, conn.id);
                break;
            case 'own-player':
                this.assignInput(new GameInput(), msg.playerId);
        }
    }

    onEvent(event) {
        switch (event.name) {
            case 'player-added':
                console.log('Adding entity...');
                this.serverAddEntity('PlayerCharacter', {owner: event.playerId});
                return;
            case 'player-removed':
                this.entities.forEach(entity => {
                    if (entity.owners.indexOf(event.playerId) > -1) self.serverRemoveEntity(entity.entityId);
                }); 
                return;
        }
    }

    ready() {
        if (this.isServer) {
            this.startGame();
        } else {
            console.log('Joining...');
            this.gateway.lastConn.send({type: 'join'});
            // Should instead read:
            // this.gateway.client.send(...
        }   
    }

    afterTick(ms) {
        this.render();
    }

    clean() {}

    startGame() {
        this.clean();
        this.serverAddEntity('World', {});
        this.addPlayer(1, null);
    }

    render() {
        render.reset();
        this.entities.forEach(entity => {
            entity.render();
        });
    }
}
